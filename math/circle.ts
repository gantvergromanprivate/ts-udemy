export const PI = 3.14;

export function calculateСircumference(diameter: number) {
  return PI * diameter;
}