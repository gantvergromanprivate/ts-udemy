function logged(constructorFn: Function) {
  console.log(constructorFn);
}

@logged
class Car {
  constructor() {
    console.log('Hi!');
  }
}