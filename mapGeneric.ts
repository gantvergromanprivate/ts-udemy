class MyMap<T> {
  items: { [propName: string]: T } = {};
  setItem(key: string, item: T): void {
    this.items[key] = item;
  }
  getItem(key: string): T {
    return this.items[key];
  } 
  clear(): void {
    this.items = {};
  }
  printMap(): void {
    Object.keys(this.items).map(key => console.log(`key=${key}, value=${this.items[key]}`));
  }
}

const numberMap = new MyMap<number>();
numberMap.setItem('aples', 5);
numberMap.setItem('bananas', 10);
numberMap.printMap();
numberMap.setItem('aples', 8);
numberMap.printMap();
console.log(numberMap.getItem('aples'));
numberMap.clear();
numberMap.printMap();
numberMap.setItem('cherry', 12);
numberMap.printMap();

const stringMap = new MyMap<string>();
stringMap.setItem('name', 'Roman');
stringMap.setItem('age', '42');
stringMap.printMap();