// Exercise 1
class Car {
  public acceleration: number;
  constructor (public name: string) {
    this.name = name;
    this.acceleration = 0;
  }
  public honk = (): void => console.log('Toooooooooot!');
  public accelerate = (speed: number) => this.acceleration += speed;
}
const car = new Car('BMW');
car.honk();
console.log(car.acceleration);
car.accelerate(10);
console.log(car.acceleration);

// Exercise 2
class BaseObject {
  public width = 0;
  public length = 0;
}
class Rectangle extends BaseObject {
  public calcSize = () => this.width * this.length;
}
const rectangle = new Rectangle();
rectangle.width = 5;
rectangle.length = 2;
console.log(rectangle.calcSize());

// Exercise 3
class Person {
  private _firstName: string = '';
  set firstName(value: string) {
    if (value.length > 3) {
      this._firstName = value;
    }
    else {
      this._firstName = '';
    }
  }
  get firstName() {
    return this._firstName;
  }
};
const person = new Person();
console.log(person.firstName);
person.firstName = 'Ma';
console.log(person.firstName);
person.firstName = 'Maximilian';
console.log(person.firstName);