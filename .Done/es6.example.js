const user = { name: 'Roman', age: 42 };

const { name: username, age: userage } = user;

const anotherUser = { name: 'Karina', age: 43 };

const { name: anotherUsername, age: anotherUserage } = anotherUser;

console.log(username, userage);
console.log(anotherUsername, anotherUserage);
