var MyMap = /** @class */ (function () {
    function MyMap() {
        this.items = {};
    }
    MyMap.prototype.setItem = function (key, item) {
        this.items[key] = item;
    };
    MyMap.prototype.getItem = function (key) {
        return this.items[key];
    };
    MyMap.prototype.clear = function () {
        this.items = {};
    };
    MyMap.prototype.printMap = function () {
        var _this = this;
        Object.keys(this.items).map(function (key) { return console.log("key=" + key + ", value=" + _this.items[key]); });
    };
    return MyMap;
}());
var numberMap = new MyMap();
numberMap.setItem('aples', 5);
numberMap.setItem('bananas', 10);
numberMap.printMap();
numberMap.setItem('aples', 8);
numberMap.printMap();
console.log(numberMap.getItem('aples'));
numberMap.clear();
numberMap.printMap();
numberMap.setItem('cherry', 12);
numberMap.printMap();
var stringMap = new MyMap();
stringMap.setItem('name', 'Roman');
stringMap.setItem('age', '42');
stringMap.printMap();
